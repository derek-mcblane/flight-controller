# Crosstool-NG Configs

[Crosstool-NG][crosstool-NG] is a tool to build cross-compilation toolchains.

This directory contains a collection of [crosstool-NG][] configuration files to
be used to build toolchains for this project. You can symlink one of these
configurations into the project root and use [crosstool-NG][] to build a
toolchain.

To build a Raspberry Pi 4 toolchain for example, you can do the following from
the project root:

```sh
ln -sf ct-ng/rpi4-config .config
ct-ng build
```

After a few minutes depending on your system, you should have a built toolchain
under the `.ct-ng/x-tools/` directory.

## Creating toolchain configurations

[Crosstool-NG][crosstool-NG] will have the most up-to-date documentation about
how to do this. For this project, it is recommended to set the following
variables appropriately in the configuration or use an existing config as a
starting point:

    CT_LOCAL_TARBALLS_DIR="${CT_TOP_DIR}/.ct-ng/tarballs"
    CT_WORK_DIR="${CT_TOP_DIR}/.ct-ng/build"
    CT_PREFIX_DIR="${CT_PREFIX:-${CT_TOP_DIR}/.ct-ng/x-tools}/${CT_HOST:+HOST-${CT_HOST}/}${CT_TARGET}"

These variables correspond to settings under the "Paths and misc options" ->
"Paths" section when using the menuconfig interface to ct-ng. If the config
variables are specified as defined above, you can expect CT-NG output data to
be under `.ct-ng/` in the project root.

[crosstool-NG]: https://crosstool-ng.github.io/docs/
